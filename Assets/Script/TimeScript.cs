using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TimeScript : MonoBehaviour
{
    bool timeActive = true;
    float currentTime;
    public Text currentTimeText;

    // Start is called before the first frame update
    void Start()
    {
        currentTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(timeActive){
            currentTime = currentTime + Time.deltaTime;
        }
        TimeSpan time = TimeSpan.FromSeconds(currentTime);
        currentTimeText.text = time.ToString(@"mm\:ss\:fff");

        if(TriangleScript.triangleComplete == true && CircleScript.circleComplete == true && DiamondScript.diamondComplete == true && ShieldScript.shieldComplete == true && SquareScript.squareComplete == true && StarScript.starComplete == true){
           timeActive = false;
        } 
    }
}
